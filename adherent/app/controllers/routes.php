<?php
Configuration::config();

$app->get('/',function() use ($app){
    $listeTypes=Types::all();
    $app->render('index.twig',['liste'=>$listeTypes]);
});

$app->get('/recherche',function() use ($app){
	$listeTypes=Types::all();
	$listeGenre=Genre::all();
	$listeEtat=Etats::all();
  $app->render('recherche.twig',['lt'=>$listeTypes, 'lg'=>$listeGenre, 'le'=>$listeEtat]);
});

$app->get('/listeDocument:id',function($id) use ($app){

  $mot='';
  $type=$id;
  $etat='';
  $genre='';
  $liste_doc=Document::recherche($mot, $type, $etat, $genre);

  $app->render('listeDocument.twig', ['documents'=>$liste_doc]);
});

$app->post('/listeDoc',function() use($app){
  $mot=filter_var($app->request->post('mot'),FILTER_SANITIZE_STRING);
  $type=filter_var($app->request->post('type'),FILTER_SANITIZE_STRING);
  $etat=filter_var($app->request->post('etat'),FILTER_SANITIZE_STRING);
  $genre=filter_var($app->request->post('genre'),FILTER_SANITIZE_STRING);
  $liste_doc=Document::recherche($mot, $type, $etat, $genre);

  $app->render('listeDocument.twig', ['documents'=>$liste_doc]);
//  $app->render('resultatRecherche.twig', ['documents'=>$liste_doc]);
});

$app->get('/fiche:id',function($id) use($app){
  $detail=Document::find($id);
  $etat=Etats::find($detail->id_etat);
   $emp=Emprunt::where('id_doc','=',$detail->id)->where('id_etat','=','3')->first();


  $app->render('fiche.twig',['detail'=>$detail,'etat'=>$etat,'emp'=>$emp]);
});




?>
