<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Etats extends Eloquent{
  protected $table = 'etats';
	protected $key='id';

  public function documents(){
    return $this->hasMany('Document');
  }
  public function emprunts(){
    return $this->hasMany('Emprunt');
  }

}
 ?>
