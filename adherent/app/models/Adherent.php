<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Adherent extends Eloquent{
  protected $table = 'adherent';
	protected $key='id';
  protected $timestamp=true;

  public function emprunt(){
    return $this->hasMany('Emprunt');
  }


  public static function ajouterAdherent($nom,$prenom,$adresse,$telephone,$email){
  	$adh=new Adherent();
  	$adh->nom=$nom;
  	$adh->prenom=$prenom;
  	$adh->adresse=$adresse;
  	$adh->telephone=$telephone;
  	$adh->email=$email;
  	if($adh->save())
  		return true;
  	else
  		return false;
  }

}

 ?>
