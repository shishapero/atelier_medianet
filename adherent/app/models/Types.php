<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Types extends Eloquent{
  protected $table = 'types';
	protected $key='id';

  public function documents(){
    return $this->hasMany('Document','id','id_type');
  }

}
 ?>
