<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Seeder;
class Document extends Eloquent{
  protected $table = 'document';
	protected $key='id';
  protected $timestamp=true;

  public function emprunt(){
    return $this->belongsTo('Emprunt');
  }

  public function type(){
    return $this->belongsTo('Types','id_type','id');
  }

  public function etat(){
    return $this->belongsTo('Etats','id_etat','id');
  }

  public function genres(){
    return $this->belongsToMany('Genre');
  }

  public static function ajouterDocument($titre,$auteur,$descriptif,$etat,$type,$genre)
  {
      $doc=new Document();
      $doc->titre=$titre;
      $doc->auteur=$auteur;
      $doc->descriptif=$descriptif;
      $doc->id_etat=$etat;
      $doc->id_type=$type;
      $doc->genres($genre);
      if($doc->save())
      {

        $doc->genres()->attach($genre);
          return "Document enregistré avec succès";

      }
      else
        return "ce document n'a pas etait enregistrer";

  }



  public static function recherche($mot, $type, $etat, $genre){
    $docs=Document::where(function($query) use($mot)
              {
                $query->where('titre', 'like', "%$mot%")
                      ->orWhere('auteur', 'like', "%$mot%")
                      ->orWhere('descriptif', 'like', "%$mot%");
              });

    if($type!=''){
        $docs->where('id_type','=', $type);
    }
    if($etat!=''){
      $docs->where('id_etat','=', $etat);
    }
  if($genre!=''){
      $docs->whereHas('genres',function($q) use($genre)
            {
                    $q->where('genre_id','=', $genre);
                });
      }
      $document=$docs->orderBy('id_etat')->get();
      $res=array();
      foreach ($document as $d){
           $r=array('documents'=>$d, 'etat'=>$d->etat()->first()->etat);
           array_push($res, $r);
        }

      return $res;

    }
    public function modifierEtatDocument($id,$etat)
  {
    $doc=Document::find($id);
    if($doc !=null){
        $doc->id_etat=$etat;
        if($doc->save())
        {
          return "Le document est maintenant :".Etats::find($etat)->etat;
        }
        else
          return "Le changement d'etat n'a pas été effectué";
    }
    else
      return "Document non trouvé !!";


  }

}
 ?>
