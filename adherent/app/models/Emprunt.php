<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Emprunt extends Eloquent{
  protected $table = 'emprunt';
	protected $key='id';
  protected $timestamp=true;

  public function adherent(){
    return $this->belongsTo('Adherent');
  }

  public function documents(){
    return $this->belongsTo('Document','id_doc','id');
  }

  public static function emprunter($id_adh,$id_doc){

    try {

      if(Document::find($id_doc)->id_etat==1)
      {

          $emp=new Emprunt();
          $emp->id_adh=$id_adh;
          $emp->id_doc=$id_doc;
          $emp->id_etat=3;

          if($emp->save())
          {
            $doc=Document::find($id_doc);
            $doc->id_etat=3;
            if($doc->save()){
              return true;}
            else

              return false;
          }
          else
            return false;
        }
     } catch (Exception $e) {

      }

  }

public static function retourner($id_doc){
  try {
        $emps=Emprunt::where('id_doc','=',$id_doc)->where('id_etat','=',3)->get();
        foreach ($emps as $emp) {
            $emp->id_etat=1;

            if($emp->save())
            {
              $doc=Document::find($id_doc);
              $doc->id_etat=1;
              if($doc->save())
                return true;
              else
                return false;
              {
                $emp->id_etat=2;
                $emp->save();
                return false;
              }
            }
            else
              return false;
        }
  } catch (Exception $e) {
          echo $e;
  }
}

  public static function listeEmprunt($id_adh){

    return Emprunt::where('id_adh','=',$id_adh)->where('id_etat','=',1);

  }

}

 ?>
