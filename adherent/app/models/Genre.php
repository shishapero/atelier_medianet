<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Genre extends Eloquent{
  protected $table = 'genre';
	protected $key='id';
  protected $timestamp=true;

  public function documents(){
  	return $this->belongsToMany('Document');
  }

}
 ?>
