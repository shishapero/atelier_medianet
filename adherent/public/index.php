<?php
require '../vendor/autoload.php';


$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig(),
    'templates.path' => '../app/templates'
));


require '../app/controllers/routes.php';

$app->run();

?>
