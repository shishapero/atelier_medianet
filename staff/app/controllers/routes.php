<?php

// DB configuration
Configuration::config();



$app->get('/',function() use ($app){
    $app->render('index.twig');
});


$app->get('/ajoutDoc',function() use ($app){
	$listeTypes=Types::all();
	$listeGenre=Genre::all();
	$listeEtat=Etats::all();
    $app->render('ajoutDoc.twig',['lt'=>$listeTypes, 'lg'=>$listeGenre, 'le'=>$listeEtat]);
});

$app->post('/enregisterDoc',function() use($app){

	$titre=$app->request->post('titre');
	$auteur=$app->request->post('auteur');
	$descriptif=$app->request->post('descriptif');
	$etat=$app->request->post('etat');
	$type=$app->request->post('type');
	$genre=$app->request->post('genre');

	$ajout=Document::ajouterDocument($titre,$auteur,$descriptif,$etat,$type,$genre);

	$app->render('index.twig',['aj'=>$ajout]);

});

$app->get('/retirerDoc',function() use($app){
	$app->render('retirerDoc.twig');
});

$app->post('/retirer',function() use($app){
	if(isset($_POST['retirer'])){
		$app->render('index.twig',['ret'=>Document::modifierEtatDocument($app->request->post('id'),2)]);
	}
	elseif(isset($_POST['remettre'])){
		$app->render('index.twig', ['ret'=>Document::modifierEtatDocument($app->request->post('id'),1)]);
	}
});


$app->get('/ajoutadh',function() use($app){
	$adh= new Adherent();
	$adh->nom='Nom';
	$adh->prenom='Prenom';
	$adh->adresse='Adresse';
	$adh->telephone='N° Téléphone';
	$adh->email='Email';
	$app->render('gestionAdh.twig',['action'=>'Ajouter','lien'=>'ajouterAdh','adh'=>$adh]);
});

$app->post('/gestionadh',function() use($app){

	$adh=Adherent::find($app->request->post('id'));
	if($adh != null){
	$app->render('gestionAdh.twig',['action'=>'Modifier','lien'=>'gererAdh','adh'=>$adh,'val'=>$adh]);}

	else
		$app->render('index.twig',['missadh'=>'Impossible de touver l\'adherent : '.$app->request->post('id')]);
});

$app->post('/ajouterAdh',function() use($app){

	$nom=$app->request->post('nom');
	$prenom=$app->request->post('prenom');
	$adresse=$app->request->post('adresse');
	$telephone=$app->request->post('telephone');
	$email=$app->request->post('email');

	$ajout=Adherent::ajouterAdherent($nom,$prenom,$adresse,$telephone,$email);

	if (!$ajout)
		$ajout="Echec de l'ajout";
	else
		$ajout="$nom $prenom Ajouté avec succès, votre numéro adhérent est $ajout";


	$app->render('index.twig',['ajAdh'=>$ajout]);

});

$app->post('/gererAdh',function() use($app){
	$adh=Adherent::find($app->request->post('id'));
	$adh->nom=$app->request->post('nom');
	$adh->prenom=$app->request->post('prenom');
	$adh->adresse=$app->request->post('adresse');
	$adh->telephone=$app->request->post('telephone');
	$adh->email=$app->request->post('email');

	if ($adh->save())
		$ajout="Modification effectuée avec succès";
	else
		$ajout="Echec de la modification";
	$app->render('index.twig',['ajAdh'=>$ajout]);

});

$app->post('/supprimerAdh',function() use($app){
	$adh=Adherent::find($app->request->post('id'));

	if ($adh->delete())
		$ajout="Suppression effectuée avec succès";
	else
		$ajout="Echec de la suppression";
	$app->render('index.twig',['ajAdh'=>$ajout]);

});

$app->get('/emprunt',function() use ($app){
    $app->render('gestionEmprunt.twig',['action'=>'Emprunter','lien'=>'emprunter']);
});

$app->get('/retour',function() use ($app){
    $app->render('gestionEmprunt.twig',['action'=>'Rendre','lien'=>'rendre']);
});


$app->post('/emprunter',function() use($app){
	$docs=$_POST['docs'];
	$adh=$_POST['adh'];
	foreach ($docs as $key => $value) {
		if(!Emprunt::emprunter($adh,$value))
		{
			$err="$value ";
		}
	}
	if(!isset($err))
		$err="Documents empruntés";
	else
		$err="Les documents suivant ne peuvent pas être emprunter : ".$err;
	$app->render('recap.twig',['docs'=>$docs,'adh'=>$adh,'err'=>$err]);
});

$app->post('/rendre',function() use($app){
	$docs=$_POST['docs'];
	foreach ($docs as $key => $value) {
		if(!Emprunt::retourner($value))
		{
			$err="$value ";
		}
	}
	if(!isset($err))
		$err="Documents rendus";
	else
		$err="Les document suivent ne peuvent pas être retourner : ".$err;
	$app->render('recap.twig',['docs'=>$docs,'adh'=>'','err'=>$err]);
});

$app->get('/ajoutType',function() use($app){
	$app->render('ajoutType.twig');
});

$app->post('/ajouterType',function() use($app){
	$type=$app->request->post('type');
	$image=$app->request->post('image');
	$ajout=Types::ajouterType($type,$image);
	if ($ajout)
		$ajout="$type Ajouté avec succès";
	else
		$ajout="Echec de l'ajout du type";

	$app->render('index.twig',['ajType'=>$ajout]);
});

$app->post('/ajoutGenre',function() use($app){
	$genre=$app->request->post('genre');
	$ajout=Genre::ajouterGenre($genre);

	if ($ajout)
		$ajout="$genre Ajouté avec succès";
	else
		$ajout="Echec de l'ajout du genre";

	$app->render('index.twig',['ajGenre'=>$ajout]);
});


?>
