<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Types extends Eloquent{
  protected $table = 'types';
	protected $key='id';
	protected $timestamp=true;

  public function document(){
    return $this->hasMany('Document');
  }

  public static function ajouterType($type,$image)
  {	
  	$tpe=new Types();
  	$tpe->type=$type;
  	$tpe->src_image=$image;
  	if ($tpe->save())
  		return true;
  	else
  		return false;
  }
}
 ?>
