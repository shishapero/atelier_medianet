<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Etats extends Eloquent{
  protected $table = 'etats';
	protected $key='id';

  public function document(){
    return $this->hasMany('Document');
  }
}
 ?>
