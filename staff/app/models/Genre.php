<?php
use Illuminate\Database\Eloquent\Model as Eloquent;

class Genre extends Eloquent{
  protected $table = 'genre';
	protected $key='id';
  protected $timestamp=true;

  public function documents(){
  	return $this->belongsToMany('Document');
  }

  public static function ajouterGenre($genre)
  {	
  	$gnr=new Genre();
  	$gnr->reference=$genre;
  	if ($gnr->save())
  		return true;
  	else
  		return false;
  }

}
 ?>
