-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Jeu 05 Novembre 2015 à 17:14
-- Version du serveur :  10.0.17-MariaDB
-- Version de PHP :  5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `medianet`
--

-- --------------------------------------------------------

--
-- Structure de la table `adherent`
--

DROP TABLE IF EXISTS `adherent`;
CREATE TABLE `adherent` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `adresse` varchar(256) NOT NULL,
  `telephone` int(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vider la table avant d'insérer `adherent`
--

TRUNCATE TABLE `adherent`;
--
-- Contenu de la table `adherent`
--

INSERT INTO `adherent` (`id`, `nom`, `prenom`, `adresse`, `telephone`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Mainville', 'Henriette', '7, boulevard Charlemagne\r\n54000 Nancy', 383647318, 'henriettemainville@armyspy.com ', '2015-11-04 19:37:36', '0000-00-00 00:00:00'),
(2, 'Sarrazin', 'Olivier', '15, rue des Ponts 54000 Nancy', 383657209, 'oliviersarrazin@armypsy.com', '2015-11-04 19:37:45', '0000-00-00 00:00:00'),
(3, 'Cyr', 'Mireille', '24, rue Chanzy 54000 Nancy', 383465365, 'mireille.cyr@gmail.com', '2015-11-04 19:37:55', '0000-00-00 00:00:00'),
(4, 'Quinn', 'Gérard', '10, rue Emile Galle', 383709013, 'gege.quinn@orange.fr', '2015-11-04 19:37:11', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `authentification`
--

DROP TABLE IF EXISTS `authentification`;
CREATE TABLE `authentification` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vider la table avant d'insérer `authentification`
--

TRUNCATE TABLE `authentification`;
-- --------------------------------------------------------

--
-- Structure de la table `document`
--

DROP TABLE IF EXISTS `document`;
CREATE TABLE `document` (
  `id` int(11) NOT NULL,
  `titre` varchar(100) NOT NULL,
  `auteur` varchar(50) NOT NULL,
  `descriptif` varchar(500) NOT NULL,
  `id_etat` int(11) NOT NULL,
  `id_type` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vider la table avant d'insérer `document`
--

TRUNCATE TABLE `document`;
--
-- Contenu de la table `document`
--

INSERT INTO `document` (`id`, `titre`, `auteur`, `descriptif`, `id_etat`, `id_type`, `created_at`, `updated_at`) VALUES
(2, 'Le seigneur des anneaux : la communauté de l''anneau', 'Peter Jackson', ' Dans ce chapitre de la trilogie Frodon Sacquet, hérite d''un anneau. Il s''agit de l''Anneau Unique, un instrument de pouvoir absolu qui permettrait au Seigneur des ténèbres, de régner sur la Terre du Milieu et de réduire en esclavage ses peuples.', 2, 2, '2015-11-05 08:06:54', '2015-11-05 08:06:54'),
(3, 'Harry Potter et la Coupe de feu', 'J.K. Rowling', 'Harry Potter et la Coupe de feu (titre original : Harry Potter and the Goblet of Fire) est le quatrième tome de la série littéraire centrée sur le personnage d''Harry Potter.', 1, 3, '2015-11-04 19:43:52', '0000-00-00 00:00:00'),
(6, 'Racine Carrée', 'Stromae', '1. Ta fête 2:56\r\n2. Papaoutai 3:52\r\n3. Bâtard 3:28\r\n4. Ave Cesaria 4:09\r\n5. Tous les mêmes 3:33\r\n6. Formidable 3:33\r\n7. Moules frites 2:38\r\n8. Carmen 3:09\r\n9. Humain à l''eau 3:59\r\n10. Quand c''est ? 3:00\r\n11. Sommeil 3:38\r\n12. Merci 3:54\r\n13. AVF (avec Maître Gims et Orelsan) 3:44', 2, 1, '2015-11-04 21:51:11', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `emprunt`
--

DROP TABLE IF EXISTS `emprunt`;
CREATE TABLE `emprunt` (
  `id` int(11) NOT NULL,
  `id_adh` int(11) NOT NULL,
  `id_doc` int(11) NOT NULL,
  `id_etat` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vider la table avant d'insérer `emprunt`
--

TRUNCATE TABLE `emprunt`;
--
-- Contenu de la table `emprunt`
--

INSERT INTO `emprunt` (`id`, `id_adh`, `id_doc`, `id_etat`, `created_at`, `updated_at`) VALUES
(2, 1, 2, 2, '2015-11-05 08:06:53', '2015-11-05 08:06:53');

-- --------------------------------------------------------

--
-- Structure de la table `etats`
--

DROP TABLE IF EXISTS `etats`;
CREATE TABLE `etats` (
  `id` int(11) NOT NULL,
  `etat` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vider la table avant d'insérer `etats`
--

TRUNCATE TABLE `etats`;
--
-- Contenu de la table `etats`
--

INSERT INTO `etats` (`id`, `etat`) VALUES
(1, 'disponible'),
(2, 'indisponible'),
(3, 'Emprunter');

-- --------------------------------------------------------

--
-- Structure de la table `genre`
--

DROP TABLE IF EXISTS `genre`;
CREATE TABLE `genre` (
  `id` int(11) NOT NULL,
  `reference` varchar(50) NOT NULL,
  `date_ajout` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vider la table avant d'insérer `genre`
--

TRUNCATE TABLE `genre`;
--
-- Contenu de la table `genre`
--

INSERT INTO `genre` (`id`, `reference`, `date_ajout`) VALUES
(1, 'fantastique', '2015-11-04 18:52:04'),
(2, 'polar', '2015-11-04 18:52:09');

-- --------------------------------------------------------

--
-- Structure de la table `genre_doc`
--

DROP TABLE IF EXISTS `genre_doc`;
CREATE TABLE `genre_doc` (
  `id_doc` int(11) NOT NULL,
  `id_genre` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vider la table avant d'insérer `genre_doc`
--

TRUNCATE TABLE `genre_doc`;
--
-- Contenu de la table `genre_doc`
--

INSERT INTO `genre_doc` (`id_doc`, `id_genre`, `created_at`, `updated_at`) VALUES
(2, 1, '2015-11-05 11:21:37', '0000-00-00 00:00:00'),
(2, 2, '2015-11-05 11:21:37', '0000-00-00 00:00:00'),
(3, 1, '2015-11-05 11:21:37', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `staff`
--

DROP TABLE IF EXISTS `staff`;
CREATE TABLE `staff` (
  `id` int(11) NOT NULL,
  `nom` int(11) NOT NULL,
  `prenom` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vider la table avant d'insérer `staff`
--

TRUNCATE TABLE `staff`;
-- --------------------------------------------------------

--
-- Structure de la table `types`
--

DROP TABLE IF EXISTS `types`;
CREATE TABLE `types` (
  `id` int(11) NOT NULL,
  `type` varchar(11) NOT NULL,
  `src_image` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Vider la table avant d'insérer `types`
--

TRUNCATE TABLE `types`;
--
-- Contenu de la table `types`
--

INSERT INTO `types` (`id`, `type`, `src_image`) VALUES
(1, 'CD', 'images/cd.jpg'),
(2, 'DVD', 'images/dvd.png'),
(3, 'Livres', 'images/livre.png');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `adherent`
--
ALTER TABLE `adherent`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `authentification`
--
ALTER TABLE `authentification`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user` (`user`),
  ADD KEY `Fk_user` (`user`);

--
-- Index pour la table `document`
--
ALTER TABLE `document`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_etat` (`id_etat`),
  ADD KEY `Ind_Type` (`id_type`) USING BTREE;

--
-- Index pour la table `emprunt`
--
ALTER TABLE `emprunt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_doc` (`id_doc`) USING BTREE,
  ADD KEY `id_adh` (`id_adh`) USING BTREE,
  ADD KEY `id_etat` (`id_etat`) USING BTREE;

--
-- Index pour la table `etats`
--
ALTER TABLE `etats`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `genre_doc`
--
ALTER TABLE `genre_doc`
  ADD PRIMARY KEY (`id_doc`,`id_genre`),
  ADD KEY `Fk_genre` (`id_genre`);

--
-- Index pour la table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `adherent`
--
ALTER TABLE `adherent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `authentification`
--
ALTER TABLE `authentification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `document`
--
ALTER TABLE `document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `emprunt`
--
ALTER TABLE `emprunt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `etats`
--
ALTER TABLE `etats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `genre`
--
ALTER TABLE `genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `types`
--
ALTER TABLE `types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `authentification`
--
ALTER TABLE `authentification`
  ADD CONSTRAINT `FK_USER_ADH` FOREIGN KEY (`user`) REFERENCES `adherent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `document`
--
ALTER TABLE `document`
  ADD CONSTRAINT `FK_etat_doc` FOREIGN KEY (`id_etat`) REFERENCES `etats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_type_doc` FOREIGN KEY (`id_type`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `emprunt`
--
ALTER TABLE `emprunt`
  ADD CONSTRAINT `FK_adh_emp` FOREIGN KEY (`id_adh`) REFERENCES `adherent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_doc_emp` FOREIGN KEY (`id_doc`) REFERENCES `document` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_etat_emp` FOREIGN KEY (`id_etat`) REFERENCES `etats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `genre_doc`
--
ALTER TABLE `genre_doc`
  ADD CONSTRAINT `FK_doc` FOREIGN KEY (`id_doc`) REFERENCES `document` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Fk_genre` FOREIGN KEY (`id_genre`) REFERENCES `genre` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
